module.exports = {
    query: query,
    queryAsync: queryAsync,
};

const Pool = require('pg').Pool;

function getPool() {
    return new Pool({
        user: 'postgres',
        host: 'localhost',
        database: 'opentime_security',
        password: 'postgres',
        port: 5432,
    });
}

function query(queryString, cbFunc) {
    const pool = getPool();
    pool.query(queryString, (error, results) => {
        cbFunc(setResponse(error, results));
    });
}

async function queryAsync(queryString) {
    const pool = getPool();
    const client = await pool.connect();
    let response = await client.query(queryString);
    setTimeout(() => {
        client.release();
    });
    return response;
}


function setResponse(error, results) {
    return {
        error: error,
        results: results ? results : null,
    };
}
