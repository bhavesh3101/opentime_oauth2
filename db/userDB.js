let pgPool;

module.exports = (injectedPgPool) => {
    pgPool = injectedPgPool;
    return {
        register: register,
        getUser: getUser,
        isValidUser: isValidUser,
    };
};

const bcryptjs = require('bcryptjs');

function register(username, password, email, firstName, lastName, cbFunc) {
    let salt = bcryptjs.genSaltSync(12);
    let bcryptHash = bcryptjs.hashSync(password, salt);
    const query = `INSERT INTO opentime_user (user_name, pswd, email, first_name, last_name) 
        VALUES ('${username}', '${bcryptHash}', '${email}', '${firstName}', '${lastName}')`;
    pgPool.query(query, cbFunc);
}

async function getUser(username, plainPass, cbFunc) {
    const getUserQuery = `SELECT * FROM opentime_user WHERE user_name = '${username}'`;
    let response = await pgPool.queryAsync(getUserQuery);
    let user = response.rows && response.rows.length === 1 ? response.rows[0] : null;
    let isValidPass = await bcryptjs.compareSync(plainPass, user.pswd);
    cbFunc(false, isValidPass ? user : null);
}

function isValidUser(username, cbFunc) {
    const query = `SELECT * FROM opentime_user WHERE user_name = '${username}'`;
    const checkUsrcbFunc = (response) => {
        const isValidUser = response.results
            ? !(response.results.rowCount > 0)
            : null;
        cbFunc(response.error, isValidUser);
    };
    pgPool.query(query, checkUsrcbFunc);
}
