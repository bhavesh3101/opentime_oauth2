# Implementing OAuth 2.0 in Node.js


Before running the app, you have to set up the Postgres database.

In that you must have 'opentime_security' database and 'opentime_user' table with below columns.

```sql
DROP TABLE IF EXISTS opentime_user";
CREATE TABLE opentime_user" (
  "id" int8 NOT NULL DEFAULT nextval('opentime_user_id_seq'::regclass),
  "account_expired" bool NOT NULL DEFAULT false,
  "account_locked" bool NOT NULL DEFAULT false,
  "credentials_expired" bool NOT NULL DEFAULT false,
  "enabled" bool NOT NULL DEFAULT true,
  "pswd" varchar(512) COLLATE "pg_catalog"."default" NOT NULL,
  "user_name" varchar(256) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(256) COLLATE "pg_catalog"."default" NOT NULL,
  "first_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "created_time" timestamptz(6) NOT NULL DEFAULT now(),
  "last_modified_time" timestamptz(6) NOT NULL DEFAULT now()
);
```

Run the command to install the npm dependencies:

```
npm install
```

And finally:

```
node index.js
```

The app will start at http://localhost:3000.


In the postman:

Use this curl to register new user

```
curl --location --request POST 'http://localhost:3000/auth/register' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=bkk1' \
--data-urlencode 'password=password' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'email=bhavesh1@mail.com' \
--data-urlencode 'firstname=Bhavesh' \
--data-urlencode 'lastname=Kadiya'
```

Use this curl to get token for that user

```
curl --location --request POST 'http://localhost:3000/auth/login' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=bkk1' \
--data-urlencode 'password=password' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'client_id=null' \
--data-urlencode 'client_secret=null'
```

Opentime-jobs project's oauth curl to get a token.

```
curl --location --request POST 'http://localhost:9001/oauth/token' \
--header 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
--header 'sec-ch-ua-mobile: ?0' \
--header 'Authorization: Basic b3BlbnRpbWUtam9icy1wcm92aWRlci13ZWI6YXBwMTIz' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Referer: http://localhost:8080/' \
--header 'X-Requested-With: XMLHttpRequest' \
--header 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'username=test1' \
--data-urlencode 'password=test'
```
